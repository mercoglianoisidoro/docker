#!/bin/bash



docker rm -f -v $(docker ps -aqf "name=couchdb*");
docker volume rm couchdbDATA;
docker volume rm supervisord_couchdbDATA;

cd couchdb
docker-compose build
echo "TEST couchdb"
docker-compose run test_couchdb
cd ..

docker rm -f -v $(docker ps -aqf "name=couchdb*");
docker volume rm couchdbDATA;
docker volume rm supervisord_couchdbDATA;



cd couchdb-supervisord
docker-compose build
echo "TEST couchdb-supervisord"
docker-compose run test_supervisor_couchdb
cd ..