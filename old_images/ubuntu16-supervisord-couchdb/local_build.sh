#!/bin/bash


#juste to test locally:

#remove and create volumes
docker volume rm supervisord_couchdbDATA
docker volume create supervisord_couchdbDATA

#remove container:
docker rm -f -v $(docker ps -aqf "name=supervisord_couchdb")

#remove  image:
docker rmi $(docker images | grep -E "supervisord_couchdb" |  awk '{ print $3; }')

#build
docker build -t supervisord_couchdb:testing .

#run
docker run -d  -p 5984:5984 -p 2222:22  \
-e COUCHDB_USER=admin -e COUCHDB_PASSWORD=admin_pass \
-e ROOT_PASSWORD=root_pass \
--mount source=supervisord_couchdbDATA,target=/data \
--name supervisord_couchdb supervisord_couchdb:testing



#to see logs:
docker logs -f $(docker ps -aqf "name=supervisord_couchdb")


#test with dgoss
#into test_dockerhub_image directory
dgoss run -d  -p 5984:5984 -p 2222:22  \
-e COUCHDB_USER=admin -e COUCHDB_PASSWORD=admin_pass \
-e ROOT_PASSWORD=root_pass \
--mount source=supervisord_couchdbDATA,target=/data \
--name goss_testing supervisord_couchdb:testing






