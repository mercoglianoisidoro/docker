FROM mercisi/supervisord:sshd
MAINTAINER Isidoro <mercogliano.isidoro@gmail.com>

# Setup environment
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update &&  apt-get upgrade -y

# ----------   deps   ----------

RUN apt-get --no-install-recommends -y install \
    build-essential pkg-config erlang \
    libicu-dev libmozjs185-dev libcurl4-openssl-dev curl jq

# ----------   COUCHDB   ----------

LABEL couchdb_version=2.3.0
# CouchDB source code:
# curl -fSL https://www-eu.apache.org/dist/couchdb/source/2.3.0/apache-couchdb-2.3.0.tar.gz  -o couchdb.tar.gz

COPY couchdb.tar.gz /usr/src/

# compile couchdb
 RUN mkdir -p /usr/src/couchdb \
     && tar -xzf /usr/src/couchdb.tar.gz -C /usr/src/couchdb --strip-components=1 \
     && cd /usr/src/couchdb && ./configure \
     && make release

# create user couchdb and copy all files in home
 RUN adduser --system  --shell /bin/bash --group --gecos "CouchDB Administrator" couchdb \
     && adduser couchdb sudo \
     && echo 'couchdb:pass' | chpasswd \
     && cp -r /usr/src/couchdb/rel/couchdb /home/couchdb
 COPY local.ini /home/couchdb/couchdb/etc
 COPY vm.args /home/couchdb/couchdb/etc

# remove sourses
RUN rm -r /usr/src/couchdb

# this will let to use host directory to store data
RUN mkdir /data
RUN chown -R couchdb:couchdb /data


# create and fix permission for couchdb files
 RUN chown -R couchdb:couchdb /home/couchdb \
     && chown -R couchdb:couchdb /data \
     && find /home/couchdb/couchdb -type d -exec chmod 0770 {} \; \
     && chmod 0644 /home/couchdb/couchdb/etc/*

VOLUME ["/data"]

EXPOSE 5984 4369 9100





#special permission to open socker
RUN addgroup --gid 3003 aid_inet && adduser couchdb aid_inet 



# ----------   supervisord   ----------
COPY supervisord.conf /etc/

COPY ./docker-entrypoint.sh /
COPY ./install.sh /
RUN chmod +x /docker-entrypoint.sh
RUN chmod +x /install.sh
CMD  ["/docker-entrypoint.sh"]



# ----------   NODEJS   ----------
#RUN apt-get install -y curl && curl -sL https://deb.nodesource.com/setup_8.x | bash - && apt-get install -y nodejs
