#!/bin/bash

#remove
docker rm -f -v $(docker ps -aqf "name=couchdb")
docker volume rm couchdbDATA

docker volume create couchdbDATA

docker rm -f -v $(docker ps -aqf "name=couchdb")
docker build -t couchdb:16.04 .

docker run -d -p 5984:5984 \
-e COUCHDB_USER=admin -e COUCHDB_PASSWORD=admin_pass \
--mount source=couchdbDATA,target=/data \
--name couchdb couchdb:16.04

docker logs -f $(docker ps -aqf "name=couchdb")

