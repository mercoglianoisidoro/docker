#!/bin/bash



db=test
userAdmin=admin
passAdmin=admin_pass
couchdb_address=localhost
COUCHDB_USER=admin
COUCHDB_PASSWORD=admin_pass
couchdb_address=localhost
unset http_proxy
unset https_proxy

curl -s "http://$couchdb_address:5985" | jq .

removeQuotesAndDiaplyResult(){
    echo "result = $1"
    result=$( echo $1 | sed -e 's/^"//' -e 's/"$//' )
    
}




#-----------------------------------
# Working with DB
# HowTo delete database
# HowTo create database
#-----------------------------------

# imposossible to create/delete without user db

result=$(curl -s -X PUT "http://$couchdb_address:5985/$db" | jq .error )
removeQuotesAndDiaplyResult $result
if [ "$result" != "unauthorized" ]; then 
    echo "problem with put -> unauthorized"
    exit 1
else
        echo "test ok"
fi

result=$(curl -s -X DELETE "http://$couchdb_address:5985/$db" | jq .error )
removeQuotesAndDiaplyResult $result
if [ "$result" != "unauthorized" ]; then 
    echo "problem with DELETE -> unauthorized"
    exit 1
else
        echo "test ok"
fi


#user admin ok

curl -s -X DELETE "http://$userAdmin:$passAdmin@$couchdb_address:5985/$db" #delete if exist
#create
result=$( curl -s -X PUT "http://$userAdmin:$passAdmin@$couchdb_address:5985/$db"  | jq .ok )
removeQuotesAndDiaplyResult $result
if [ "$result" != "true" ]; then 
    echo "problem in creating db with user admin"
    exit 1
else
        echo "test ok"
fi

#delete
curl -s -X DELETE "http://$userAdmin:$passAdmin@$couchdb_address:5985/$db" #delete if exist
result=$( curl -s -X DELETE "http://$userAdmin:$passAdmin@$couchdb_address:5985/$db"  | jq .error )
removeQuotesAndDiaplyResult $result
if [ "$result" != "not_found" ]; then 
    echo "problem in delete db with user admin"
    exit 1l
else
        echo "test ok"
fi


#-----------------------------------
# Working with documents
# HowTo add documents
# HowTo delete documents
#-----------------------------------

#if we got here, we can create and delete db
curl -s -X DELETE "http://$userAdmin:$passAdmin@$couchdb_address:5985/$db"

#database doesn't exist
result=$(curl -s -X DELETE "http://$couchdb_address:5985/test_db_non_existing/document_id" -d '{"document_name":"document test"}' | jq .error )
removeQuotesAndDiaplyResult $result
if [ "$result" != "not_found" ]; then 
    echo "got = $result"
    echo "problem delete -> not_found"
    exit 1
else
        echo "test ok"
fi

#database doesn't exist - with user admin 
result=$(curl -s -X DELETE "http://$userAdmin:$passAdmin@$couchdb_address:5985/test_db_non_existing/document_id" -d '{"document_name":"document test"}' | jq .error )
removeQuotesAndDiaplyResult $result
if [ "$result" != "not_found" ]; then 
    echo "got = $result"
    echo "problem delete (user admin) -> not_found"
    exit 1
else
        echo "test ok"
fi



curl -s -X PUT "http://$userAdmin:$passAdmin@$couchdb_address:5985/$db"
#now we have the db

#until now DB is non protected by users, so it's public
result=$(curl -s -X PUT "http://$couchdb_address:5985/$db/document_id" -d '{"document_name":"document test"}' | jq .ok )
removeQuotesAndDiaplyResult $result
if [ "$result" != "true" ]; then 
    echo "got = $result"
    echo "problem creating document"
    exit 1
else
        echo "test ok"
fi


result=$(curl -s -X PUT "http://$userAdmin:$passAdmin@$couchdb_address:5985/$db/document_id_withUserAdmin" -d '{"document_name":"document test"}' | jq .ok )
removeQuotesAndDiaplyResult $result
if [ "$result" != "true" ]; then 
    echo "got = $result"
    echo "problem creating document with user admin"
    exit 1
else
        echo "test ok"
fi


result=$(curl -s -X PUT "http://$userAdmin:$passAdmin@$couchdb_address:5985/$db/document_id_withUserAdmin" -d '{"document_name":"document test"}' | jq .error )
removeQuotesAndDiaplyResult $result
if [ "$result" != "conflict" ]; then 
    echo "got = $result"
    echo "problem creating document: should fail"
    exit 1
else
        echo "test ok"
fi

exit 0

#curl -s -X PUT "http://$couchdb_address:5985/$db/document_id2" -d "{"document_name":"document test"}" | jq .

curl -s -X DELETE "http://$userAdmin:$passAdmin@$couchdb_address:5985/$db"








#-----------------------------------
# managing users and user right
# HowTo create user
# HowTo provide rights to db
#-----------------------------------

#create user

# first step create _user DB
curl -s -X DELETE "http://$userAdmin:$passAdmin@$couchdb_address:5985/_users" | jq
curl -s -X PUT "http://$userAdmin:$passAdmin@$couchdb_address:5985/_users" | jq

# then the add user with USERNAME:PASS
result=$(
curl -s -X PUT "http://$userAdmin:$passAdmin@$couchdb_address:5985/_users/org.couchdb.user:USERNAME" \
-H "Accept: application/json" \
-H "Content-Type: application/json" \
-d '{"name":"USERNAME","password":"PASS","roles":[],"type":"user"}'  | jq .ok
)
removeQuotesAndDiaplyResult $result
if [ "$result" != "true" ]; then 
    echo "got = $result"
    echo "problem creating user"
    exit 1
else
        echo "test ok"
fi

#add rights to $db
#fist step create db
curl -s -X PUT "http://$userAdmin:$passAdmin@$couchdb_address:5985/$db"
#add right
result=$(
curl -s -X PUT "http://$userAdmin:$passAdmin@$couchdb_address:5985/$db/_security" \
-H "Content-Type: application/json" \
-d '{"admins":{"names":[],"roles":[]},"members":{"names":["USERNAME"],"roles":[]}}' | jq .ok
)
removeQuotesAndDiaplyResult $result
if [ "$result" != "true" ]; then 
    echo "got = $result"
    echo "problem doing right to db"
    exit 1
else
        echo "test ok"
fi

#now we can't access to db in public mode
result=$(curl -s -X PUT "http://$couchdb_address:5985/$db/document_id" -d '{"document_name":"document test"}' | jq .error )
removeQuotesAndDiaplyResult $result
if [ "$result" != "unauthorized" ]; then 
    echo "got = $result"
    echo "problem: db still public mode?"
    exit 1
else
        echo "test ok"
fi

#but user USERNAME:PASS can access
result=$(curl -s -X PUT "http://USERNAME:PASS@$couchdb_address:5985/$db/document_id_USERNAME" -d '{"document_name":"document test"}' | jq .ok )
removeQuotesAndDiaplyResult $result
if [ "$result" != "true" ]; then 
    echo "got = $result"
    echo "problem: can't access with  USERNAME:PASS"
    exit 1
else
        echo "test ok"
fi


curl -s -X DELETE "http://$userAdmin:$passAdmin@$couchdb_address:5985/_users" | jq
