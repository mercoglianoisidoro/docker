#!/bin/bash

set -e



if [ "$COUCHDB_USER" ] && [ "$COUCHDB_PASSWORD" ]; then
	echo "creating user and pass for admin"
	printf "[admins]\n%s = %s\n" "$COUCHDB_USER" "$COUCHDB_PASSWORD" >> /home/couchdb/couchdb/etc/local.ini
fi


if [ -w "/data" ] 
then
    echo "Write permission is granted on /data"
    echo "starting couchdb"
    /install.sh 2>&1 &
    /home/couchdb/couchdb/bin/couchdb 2>&1
else
    echo "Write permission is NOT granted on /data"
    exit -1
fi
