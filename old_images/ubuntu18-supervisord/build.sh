#!/bin/bash

#remove:
# docker rm -f -v $(docker ps -aqf "name=ubuntu16-supervisord")
# docker rmi $(docker images | grep -E "ubuntu16-supervisord" |  awk '{ print $3; }')

docker build -t ubuntu18-supervisord:base .
docker run -d -p 2200:22 --name ubuntu18-supervisord ubuntu18-supervisord:base
