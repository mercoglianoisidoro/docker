Description
========


The image provide is build upon ubuntu:16.04, and it provide:

- supervisord
- ssh service configured with supervisord

The image is ideal to use in testing composition services and as base for other services

User: root

## SSHS
It is configured by the environement variable:
- ROOT_PASSWORD


## Run the image:
```bash 
docker run -d -p 2200:22 -e ROOT_PASSWORD=root_pass --name ubuntu16-supervisord mercisi/supervisord:sshd
```

## Test with dgoss
In "test_dockerhub_image" directory:
```bash 
dgoss run -d -p 2200:22 -e ROOT_PASSWORD=root_pass --name dgoss_testing mercisi/supervisord:sshd
```
You need dgoss installed in your system.