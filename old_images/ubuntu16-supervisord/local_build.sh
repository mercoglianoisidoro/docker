#!/bin/bash


#juste to test locally:

#remove container:
docker rm -f -v $(docker ps -aqf "name=ubuntu16-supervisord")

#remove image:
#docker rmi $(docker images | grep -E "ubuntu16-supervisord" |  awk '{ print $3; }')

#build
docker build -t ubuntu16-supervisord:base .

#test with dgoss
#into test_dockerhub_image directory
#dgoss run -d -p 2200:22 -e ROOT_PASSWORD=root_pass --name ubuntu16-supervisord_test ubuntu16-supervisord:base


#run
docker run -d -p 2200:22 -e ROOT_PASSWORD=root_pass --name ubuntu16-supervisord ubuntu16-supervisord:base
