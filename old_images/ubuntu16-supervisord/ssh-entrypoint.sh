#!/bin/bash

set -e



if [ "$ROOT_PASSWORD" ]; then
	# Create root pass for ssh service
	echo "root:$ROOT_PASSWORD" | chpasswd
	echo "root password set"
   exit 0
else
   echo 'No password set for service sshd for user root; please check env variables'
fi
